# Développement en local

## prérequis
- python et pip installés

## Installation
```
# récupération du code source
git clone https://gitlab.com/Imotekh/macdonalgue.git
cd macdonalgue

# vérifier que les dépendances sont installées
pip install -r ./requirements/development.txt

# mettre à jour le modèle de bdd et installer (sqlite)
cd src
python3 manage.py makemigrations --settings=macdonalgue.settings.development
python3 manage.py migrate --settings=macdonalgue.settings.development

# chargement des données
python3 manage.py loaddata ../db_dump/data.json --settings=macdonalgue.settings.development
# création d'un super utilisateur (backoffice)
python3 manage.py createsuperuser --settings=macdonalgue.settings.development

# préparer les media
python3 manage.py collectstatic --settings=macdonalgue.settings.development

# dezipper macdonalgue/db_dump/media.zip dans macdonalgue/src/macdonalgue, cette action doit créer un dossier media
```

## Lancement du serveur
```
# vérifier que les dépendances sont installées
python3 manage.py runserver --settings=macdonalgue.settings.development
```

## Utilisation
- site : http://127.0.0.1:8000
- backoffice : http://127.0.0.1:8000/admin

# Règles de developpement / contenu

## Règles de saisie
- Photos :
    - Nommage pour les images : model_nom_xxx (ex : recette_mousse_001, algue_dulse_001)
    - Mettre des noms d'images parlant (pas 00sq56d56sqd55sq6sd.png)
- Algues
    - Quand vous faites une fiche vérifier la saisie et le rendu sur le site
    - Ne pas mettre de ```<p>``` dans les descriptions (sinon à voir avec dev)
    - Le séparateur pour les champs catégories est le ";"
    - Les valeurs pour les disponibilités sont 1 (conseillé) 2 (possible) et (non dispo)
    - Le format des nutriments à changé "nutriment;qté;%qualité" => 4 colonnes au lieu de 3


## Problème de synchronisation local / server
Nécéssite de faire
```
# se connecter
cd /www/macdonalgue/src
python3 manage.py makemigrations --merge
```

## Mobile debbug
Ajouter ce code en fin de page HTML
```
<!-- https://github.com/liriliri/eruda -->
<script src="//cdn.jsdelivr.net/npm/eruda"></script>
<script>eruda.init();</script>
```