"""
La configuration à utiliser pour les environnements de production
"""
from dotenv import load_dotenv

from macdonalgue.settings.base import *

load_dotenv(verbose=True, override=True, dotenv_path='.env')

DEBUG = False

SECRET_KEY = os.environ.get('TOKEN_PROD')

ALLOWED_HOSTS = ['*']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '[{asctime}][{levelname}] {message}',
            'style': '{',
        },
    },
    'handlers': {
        'file': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': 'access.log',
            'formatter': 'verbose',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['file'],
            'level': 'INFO',
            'propagate': True,
        },
    },
}

STATIC_URL = '/static/'
STATIC_ROOT = '/www/macdonalgue/static/'

MEDIA_URL = '/media/'
MEDIA_ROOT = '/www/macdonalgue/media/'