"""
La configuration à utiliser pour les environnements de test
"""

from macdonalgue.base import *

DEBUG = True

SECRET_KEY = 'uehfj@070q!sexgtk4&5r6hho&*2&1j9vjyk#jjxe22m2=(3(y'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'blog_ci',
        'USER': 'postgres',
	    'PASSWORD': '',
        'HOST': 'postgres',
        'PORT': '5432',
    }
}

