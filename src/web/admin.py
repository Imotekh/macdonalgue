from django.contrib import admin
from django.utils.safestring import mark_safe

from web.models import Algue, Photo, Plage, Recette

admin.site.site_header = 'Consomalgues'
admin.site.site_title = 'Consomalgues'
admin.site.index_title = 'Administration'

@admin.register(Algue)
class AdminAlgue(admin.ModelAdmin):
    def headshot_images(self, obj):
        htmlImg = ''
        for img in obj.photos.all():
            htmlImg +=  '<img src="/media/{url}" width="200" />'.format(
                url = img.image
            )
        return mark_safe(htmlImg)
    def headshot_miniature(self, obj):
        return mark_safe('<img src="/media/{url}" width="200" />'.format(
            url = obj.miniature.image
            )
        )
    def headshot_image_emplacement_estran(self, obj):
        return mark_safe('<img src="/media/{url}" width="200" />'.format(
            url = obj.emplacement_estran.image
            )
        )
    def headshot_image_silhouette_forme(self, obj):
        return mark_safe('<img src="/media/{url}" width="200" />'.format(
            url = obj.silhouette_forme.image
            )
        )
    def headshot_image_recolte_position_coupe(self, obj):
        return mark_safe('<img src="/media/{url}" width="200" />'.format(
            url = obj.recolte_position_coupe.image
            )
        )
    list_display = ('nom', 'slug')
    list_filter = ('nom',)
    ordering = ('nom',)
    search_fields = ('nom',)
    fields = ('nom', 'nom_scientifique', 'couleur', 'categories', 'funfact', 'miniature', 'headshot_miniature', 'photos', 'headshot_images', 'substrat', 'taille_adulte', 'emplacement_estran', 'headshot_image_emplacement_estran', 'silhouette_forme', 'headshot_image_silhouette_forme', 'description', 'habitat', 'disponibilite', 'periode_recolte_legale', 'recolte_taille','recolte_position_coupe', 'headshot_image_recolte_position_coupe', 'recolte_description', 'nutrition_categories', 'nutrition_tableau','nutrition_description','bibliographie', 'publier',)
    readonly_fields = ['headshot_miniature', 'headshot_images', 'headshot_image_emplacement_estran', 'headshot_image_silhouette_forme', 'headshot_image_recolte_position_coupe']

@admin.register(Photo)
class AdminPhoto(admin.ModelAdmin):
    def headshot_image(self, obj):
        return mark_safe('<img src="/media/{url}" width="200" />'.format(
            url = obj.image
            )
        )
    list_display = ('nom',)
    list_filter = ('nom',)
    ordering = ('nom',)
    search_fields = ('nom',)
    fields = ('nom', 'image', 'headshot_image')
    readonly_fields = ['headshot_image']



@admin.register(Plage)
class AdminPlage(admin.ModelAdmin):

    list_display = ('nom', 'adresse', 'latitude', 'longitude', 'qualite')
    list_filter = ('nom', 'adresse',)
    ordering = ('nom', 'adresse',)
    search_fields = ('nom', 'adresse',)
    fields = ('nom', 'adresse', 'latitude', 'longitude', 'algues', 'qualite', 'publier', 'auteur_name', 'auteur_web', 'auteur_mail',)

@admin.register(Recette)
class AdminRecette(admin.ModelAdmin):
    def headshot_images(self, obj):
        htmlImg = ''
        for img in obj.photos.all():
            htmlImg +=  '<img src="/media/{url}" width="200" />'.format(
                url = img.image
            )
        return mark_safe(htmlImg)
    def headshot_miniature(self, obj):
        return mark_safe('<img src="/media/{url}" width="200" />'.format(
            url = obj.miniature.image
            )
        )
    list_display = ('nom', 'slug', 'temps', 'difficulte', 'type')
    list_filter = ('nom',)
    ordering = ('nom',)
    search_fields = ('nom',)
    fields = ('nom', 'miniature', 'headshot_miniature', 'photos', 'headshot_images', 'algues', 'personnes', 'temps', 'difficulte', 'type', 'ingredients', 'ustensiles', 'etapes', 'publier', 'auteur_name', 'auteur_web', 'auteur_mail',)
    readonly_fields = ['headshot_miniature', 'headshot_images']