# Generated by Django 2.2.16 on 2020-10-10 16:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0005_plage_qualite'),
    ]

    operations = [
        migrations.AlterField(
            model_name='nutriment',
            name='indice',
            field=models.CharField(blank=True, choices=[('1', '*'), ('2', '**'), ('3', '***')], max_length=10, null=True),
        ),
    ]
