from django.db import models

from slugify import slugify

class Photo(models.Model):

    nom = models.CharField(max_length=200)
    modified_date = models.DateTimeField(auto_now=True)
    image = models.ImageField(upload_to="image/")
    description = models.TextField(max_length=2000, null=True, blank=True)

    def __str__(self):
        return self.nom

class Algue(models.Model):
    ALGUE_TYPES = (
        ('V', 'verte'),
        ('B', 'brune'),
        ('R', 'rouge'),
    )
    SUBSTRAT_TYPES = (
        ('R', 'Rocheux'),
        ('S', 'Sableux'),
    )

    slug = models.SlugField(max_length=200, unique=True)
    nom = models.CharField(max_length=200, null=True, blank=False)
    nom_scientifique = models.CharField(max_length=200, null=True, blank=False)
    modified_date = models.DateTimeField(auto_now=True)
    couleur = models.CharField(max_length=16, choices=ALGUE_TYPES, null=True, blank=False)
    categories = models.TextField(max_length=200, null=True, blank=True)
    miniature = models.ForeignKey(Photo, on_delete=models.SET_NULL, null=True, blank=True, related_name="algue_miniature")
    photos = models.ManyToManyField(Photo)
    funfact = models.TextField(max_length=2000, null=True, blank=True)
    # identification
    substrat = models.CharField(max_length=16, choices=SUBSTRAT_TYPES, null=True, blank=True)
    taille_adulte = models.CharField(max_length=200, null=True, blank=True)
    emplacement_estran = models.ForeignKey(Photo, on_delete=models.SET_NULL, null=True, blank=True, related_name="algue_emplacement_estran")
    silhouette_forme = models.ForeignKey(Photo, on_delete=models.SET_NULL, null=True, blank=True, related_name="algue_silhouette_forme")
    description = models.TextField(max_length=2000, null=True, blank=True)
    habitat = models.TextField(max_length=2000, null=True, blank=True)
    # bonnes pratiques récoltes
    disponibilite = models.CharField(max_length=200, null=True, blank=True)
    periode_recolte_legale = models.CharField(max_length=200, null=True, blank=True)
    recolte_taille = models.CharField(max_length=200, null=True, blank=True)
    recolte_position_coupe = models.ForeignKey(Photo, on_delete=models.SET_NULL, null=True, blank=True, related_name="algue_recolte_position_coupe")
    recolte_description = models.TextField(max_length=2000, null=True, blank=True)
    # nutriments
    nutrition_categories = models.TextField(max_length=200, null=True, blank=True)
    nutrition_tableau = models.TextField(max_length=2000, null=True, blank=True)
    nutrition_description = models.TextField(max_length=2000, null=True, blank=True)
    # autres
    bibliographie = models.TextField(max_length=2000, null=True, blank=True)
    publier = models.BooleanField(default=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.nom)
        return super().save(*args, **kwargs)

    def __str__(self):
        return  "{} ({})".format(self.nom, self.nom_scientifique)

class Plage(models.Model):

    nom = models.CharField(max_length=200)
    modified_date = models.DateTimeField(auto_now=True)
    adresse = models.CharField(max_length=400, null=True, blank=True)
    latitude = models.FloatField()
    longitude = models.FloatField()
    algues = models.ManyToManyField(Algue, blank=True)
    qualite = models.BooleanField(default=True)
    publier = models.BooleanField(default=True)
    auteur_name = models.CharField(max_length=200, null=True, blank=False, default="Consomalgues")
    auteur_mail = models.CharField(max_length=200, null=True, blank=True, default="consomalgues@gmail.com")
    auteur_web = models.CharField(max_length=200, null=True, blank=True, default="https://www.consomalgues.fr/")

    def __str__(self):
        return self.nom

class Recette(models.Model):

    RECETTE_TYPES = (
        ('A', 'Apéritif'),
        ('E', 'Entrée'),
        ('P', 'Plat'),
        ('D', 'Dessert'),
    )

    DIFFICULTE = (
        ('F', 'Facile'),
        ('M', 'Moyenne'),
        ('D', 'Difficile'),
    )

    nom = models.CharField(max_length=200)
    slug = models.SlugField(max_length=200, unique=True)
    modified_date = models.DateTimeField(auto_now=True)
    miniature = models.ForeignKey(Photo, on_delete=models.SET_NULL, null=True, blank=True, related_name="recette_miniature")
    photos = models.ManyToManyField(Photo)
    algues = models.ManyToManyField(Algue, blank=True)
    personnes = models.IntegerField()
    temps = models.CharField(max_length=200)
    type = models.CharField(max_length=200, choices=RECETTE_TYPES)
    difficulte = models.CharField(max_length=200, choices=DIFFICULTE)
    ingredients = models.TextField(max_length=2000, null=True, blank=True)
    ustensiles = models.TextField(max_length=2000, null=True, blank=True)
    etapes = models.TextField(max_length=2000, null=True, blank=True)
    auteur_name = models.CharField(max_length=200, null=True, blank=False, default="Consomalgues")
    auteur_mail = models.CharField(max_length=200, null=True, blank=True, default="consomalgues@gmail.com")
    auteur_web = models.CharField(max_length=200, null=True, blank=True, default="https://www.consomalgues.fr/")
    publier = models.BooleanField(default=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.nom)
        return super().save(*args, **kwargs)

    def __str__(self):
        return self.nom
