var map = null;
var markers = [];
var serverPort = location.port != '' ? ':' + location.port : '';
var server = window.location.protocol + '//' + window.location.hostname + serverPort + '/';
console.log('Current server', server);
const openPage = (event, page) => {
    event.preventDefault();
    window.top.location = page;
};

/*
* Mapbox
*/

function initMapbox() {
    mapboxgl.accessToken = 'pk.eyJ1Ijoia2FvbmxvaCIsImEiOiJjaXVpZTg1b2swMGxlMnlsMzV3dzdvMjB2In0.SQcKuWJsbr93bMJjZYk5XA';
    map = new mapboxgl.Map({
        container: 'map',
        zoom: 6.2,
        center: [-3.2, 48.1],
        style: 'mapbox://styles/mapbox/satellite-v9',
        pitch: 0,
        bearing: -15
    });
    console.log('mapbox init');

    loadOnMapbox();
}

function getData(url) {
    return fetch(url)
        .then(response => {
            return response.json();
        })
}

async function loadOnMapbox() {

    let results = await getData(`${server}get-plages/`);
    console.log(results)

    var elColorNormal = document.createElement('div');
    elColorNormal.className = 'marker-pollution';
    elColorNormal.style.backgroundImage = `url(${server}static/img/place-color-normal.svg)`;
    elColorNormal.style.width = '40px';
    elColorNormal.style.height = '50px';

    results.forEach(element => {
        // const cloneEl = element.qualite ? elVert.cloneNode() : elRouge.cloneNode();
        const cloneEl = elColorNormal.cloneNode();
        var marker = new mapboxgl.Marker(cloneEl)
            .setLngLat([element.latitude, element.longitude])
            .addTo(map);
        marker.nom = element.nom;
        marker.nom_scientifique = element.nom_scientifique;
        marker.species = element.species;
        marker.thumbnail = element.thumbnail;
        marker.get_couleur_display = element.get_couleur_display;
        marker.modified_date = element.modified_date;
        marker.harvest = element.harvest
        markers.push(marker);
    });
    // console.log(markers)

    map.on("click", function (e) {
        markers.forEach(element => {
            // console.log('click', e.originalEvent.target, element.getElement())
            if (e.originalEvent.target === element.getElement()) {
                document.getElementById('detail-beach').style.display = 'block';
                document.getElementById('detail-beach-content').innerHTML = element.nom;

                document.getElementById('detail-beach-maree').innerHTML = "Cette fiche a été mise à jour le " + element.modified_date;
                // Le meilleur horaire pour récolter aujourd'hui des algues sur la plage de " + element.nom + " est entre 17:18 et 21:18.";

                if (element.species.length > 0) {
                    const speciesDiv = element.species.map((e, i) => {
                        const colorLine = (i%2 == 0 ? 'lightItem' : 'darkItem');
                        const harvestHTML = `
                        <div class="harvest">
                            <span class="w3-tag w3-small seaweed-color-${e.get_couleur_display}">#RécolteNow!</span>
                        </div>`;

                        return `
                        <div class="espece">
                            <a href="/algue/${e.slug}" class="w3-button w3-padding-0 w3-margin-0 flex-line-list ${colorLine}">

                                <div class="seaweed-color-${e.get_couleur_display}-liseret">
                                    <img class="thumbail" src="${e.thumbnail}"/>
                                </div>
                                <div class="listContentAlgue">
                                    <div class="listContent">
                                        <span><b>${e.nom}</b></span>
                                        <span class="small-text"><i>${e.nom_scientifique}</i></span>
                                    </div>
                                    ${ e.harvest ? harvestHTML : '' }
                                </div>

                            </a>
                        </div>
                        `;
                    });
                    document.getElementById('detail-beach-species').innerHTML = speciesDiv.join("");
                } else {
                    const msgNoSpecies = "Nous n'avons pas répertoriés les espèces présentes sur ce site. Mais selon nous, nulle doute qu'il regorge de richesses ! Petit.e veinard.e, tel un aventurier, tu vas pouvoir découvrir un territoire vierge. Si tu le souhaites,tu peux aussi nous envoyer tes observations :)";
                    document.getElementById('detail-beach-species').innerHTML = msgNoSpecies;
                }
            }
        })
    });
}

/* Manage image for recepies
function loadImage() {
    var container = document.getElementById("images");

    for (let i = 0; i < images.length; i++) {
        let img = document.createElement('img');
        img.id = images[i];
        img.src = images[i];
        img.className = 'image-tour';
        if (i > 0) img.style = 'display: none;';
        img.addEventListener('click', nextImage);
        container.appendChild(img);
    }
}
*/

function nextImage() {
    currentImage = currentImage < images.length - 1 ? currentImage + 1 : 0;
    var x = document.getElementsByClassName("image-tour");
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    document.getElementById(images[currentImage]).style.display = "block";
}


/* Manage seaweed functionnality */

function createCycle(months) {
    const monthsCycleArray = months.split(";");
    const cycleLabel = ["J", "F", "M", "A", "M", "J", "J", "A", "S", "O", "N", "D"]
    const cycleHtml = monthsCycleArray.map((e, i) => {
        return `<td class="cycle-${e}">${cycleLabel[i]}</td>`;
    })

    var cycleElement = document.getElementById("cycle");
    cycleElement.innerHTML = `<table class="cycle"><tr>${cycleHtml.join("")}</tr></table>`;
}

var lastId = 'tab-1-icon';
function changeTab(contentId, title) {
    let i;
    let x = document.getElementsByClassName("content-specie");
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    document.getElementById(contentId).style.display = "block";

    let lastIcon = document.getElementById(lastId);
    if (lastIcon) {
        lastIcon.classList.remove('main-color-normal');
    }

    let currentIconId = `${contentId}-icon`;
    let currentIcon  = document.getElementById(currentIconId);
    if (currentIcon) {
        currentIcon.classList.add('main-color-normal');
    }
    lastId = currentIconId;

    let titleElement = document.getElementById('onglet');
    titleElement.innerHTML = title;
}

/* Manage slideshow for seaweed */

function nextImg(n) {
    showImg(slideIndex += n);
}

function showImg(n) {
    var i;
    var imgs = document.getElementsByClassName("img-slide-seaweed");
    var dots = document.getElementsByClassName("dot-img-seaweed");
    if (n > imgs.length) {slideIndex = 1}
    if (n < 1) {slideIndex = imgs.length}
    for (i = 0; i < imgs.length; i++) {
        imgs[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" w3-white", "");
    }
    imgs[slideIndex-1].style.display = "block";
    dots[slideIndex-1].className += " w3-white";
}

// https://gist.github.com/SleepWalker/da5636b1abcbaff48c4d
function handleGesture() {
    const xDelta = touchstartX - touchendX;
    // const yDelta = touchstartY - touchendY;
    if (xDelta < 0) {
        nextImg(-1);
    } else {
        // right
        nextImg(1)
    }
}

function checkUncheckStep(id) {
    var element = document.getElementById(id);
    if (element) {
        if (element.src.includes('checkbox-nocheck-color-vert.png')) {
            element.src = element.src.replace('checkbox-nocheck-color-vert.png', 'checkbox-checked-color-vert.png')
        } else {
            element.src = element.src.replace('checkbox-checked-color-vert.png', 'checkbox-nocheck-color-vert.png')
        }
    }
}

// Used to detect whether the users browser is an mobile browser
function isMobile() {
    ///<summary>Detecting whether the browser is a mobile browser or desktop browser</summary>
    ///<returns>A boolean value indicating whether the browser is a mobile browser or not</returns>

    if (sessionStorage.desktop) // desktop storage
        return false;
    else if (localStorage.mobile) // mobile storage
        return true;

    // alternative
    var mobile = ['iphone','ipad','android','blackberry','nokia','opera mini','windows mobile','windows phone','iemobile'];
    for (var i in mobile) if (navigator.userAgent.toLowerCase().indexOf(mobile[i].toLowerCase()) > 0) return true;

    // nothing found.. assume desktop
    return false;
}



console.log('load consomalgues.js');
console.log('User Agent: ' + navigator.userAgent)
