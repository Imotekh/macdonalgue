from django.urls import path

from web.views import home, identifier, trouver, algue, cuisiner, recette, galerie, plage, get_plages, menu, apropos

urlpatterns = [
	path('', menu),
	path('menu/', menu),
	path('identifier/', identifier),
	path('trouver/', trouver),
	path('algue/<slug:slug>/', algue),
	path('cuisiner/', cuisiner),
	path('recette/<slug:slug>/', recette),
	path('galerie/', galerie),
	path('plage/', plage),
	path('get-plages/', get_plages),
	path('apropos/', apropos),
]