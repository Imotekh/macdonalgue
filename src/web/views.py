from django.http import JsonResponse
from django.shortcuts import render

from web.models import Plage, Algue, Recette
from datetime import datetime


def home(request):
	return render(request, 'home.html')


def menu(request):
	return render(request, 'menu.html')


def identifier(request):
	algues = Algue.objects.filter(publier=True).order_by('nom')
	currentMonth = datetime.today().month - 1
	for algue in algues:
		dispo = algue.disponibilite.split(';')
		algue.harvest = dispo[currentMonth] == '1'


	return render(request, 'identifier.html', locals())


def get_plages(request):
	plages = Plage.objects.filter(publier=True)
	data = []
	currentMonth = datetime.today().month - 1
	for plage in plages:

		species = []
		for algue in plage.algues.all():
			photo = algue.photos.all().first()
			dispo = algue.disponibilite.split(';')
			specie = {
				"nom": algue.nom,
				"slug": algue.slug,
				"nom_scientifique": algue.nom_scientifique,
			    "thumbnail": photo.image.url,
				"get_couleur_display": algue.get_couleur_display(),
				"harvest": dispo[currentMonth] == '1'
			}
			species.append(specie)

		dataFormatted = ''
		if (plage.modified_date):
			dataFormatted = plage.modified_date.strftime("%d / %m / %Y")

		data.append({
			"nom": plage.nom,
			"latitude": plage.latitude,
			"longitude": plage.longitude,
			"qualite": plage.qualite,
			"id": plage.id,
			"species": species,
			"modified_date": dataFormatted
		})
	return JsonResponse(data, safe=False)

def algue(request, slug):
	algue= Algue.objects.get(slug=slug)

	nutriments = []
	if (algue.nutrition_tableau):
		for nutrition in algue.nutrition_tableau.split("\n"):
			cells = nutrition.split(';')
			line = []
			for cell in cells:
				line.append(cell)
			nutriments.append(line)

	categorieTags = []
	if algue.categories:
		categorieTags = algue.categories.split(';')

	categorieNutritionTags = []
	if algue.nutrition_categories:
		categorieNutritionTags = algue.nutrition_categories.split(';')

	recettes= algue.recette_set.all().order_by('nom')

	photos= algue.photos.all()


	return render(request, 'algue.html', locals())

def trouver(request):
	return render(request, 'trouver.html')

def cuisiner(request):
	recettes= Recette.objects.filter(publier=True).order_by('nom')
	return render(request, 'cuisiner.html', locals())

def recette(request, slug):
	recette= Recette.objects.get(slug=slug)
	ingredients= recette.ingredients.split("\n")
	etapes= recette.etapes.split("\n")
	photos= recette.photos.all()
	return render(request, 'recette.html', locals())

def galerie(request):
	return render(request, 'galerie.html')

def plage(request):
	return render(request, 'plage.html')

def apropos(request):
	return render(request, 'apropos.html')
